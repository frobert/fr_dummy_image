<?php

########################################################################
# Extension Manager/Repository config file for ext "fr_dummy_image".
#
# Auto generated 21-03-2012 23:45
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Dummy Image',
	'description' => 'Generates dummy images into fileadmin/user_upload/dummy_images folder.
Thanks to http://dummyimage.com/',
	'category' => 'module',
	'shy' => 0,
	'version' => '1.0.1',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Robert Ferencek',
	'author_email' => 'robert@frstudio.si',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:11:{s:9:"ChangeLog";s:4:"b8fb";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"50d2";s:14:"ext_tables.php";s:4:"ec6e";s:19:"doc/wizard_form.dat";s:4:"5731";s:20:"doc/wizard_form.html";s:4:"76b0";s:13:"mod1/conf.php";s:4:"3bc2";s:14:"mod1/index.php";s:4:"f73b";s:18:"mod1/locallang.xml";s:4:"d2d4";s:22:"mod1/locallang_mod.xml";s:4:"ee87";s:19:"mod1/moduleicon.gif";s:4:"50d2";}',
);

?>