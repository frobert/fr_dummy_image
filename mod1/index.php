<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Robert Ferencek <robert@frstudio.si>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */


$LANG->includeLLFile('EXT:fr_dummy_image/mod1/locallang.xml');
require_once(PATH_t3lib . 'class.t3lib_scbase.php');
$BE_USER->modAccess($MCONF,1);	// This checks permissions and exits if the users has no permission for entry.
	// DEFAULT initialization of a module [END]



/**
 * Module 'Dummy Image' for the 'fr_dummy_image' extension.
 *
 * @author	Robert Ferencek <robert@frstudio.si>
 * @package	TYPO3
 * @subpackage	tx_frdummyimage
 */
class  tx_frdummyimage_module1 extends t3lib_SCbase {
				var $pageinfo;

				/**
				 * Initializes the Module
				 * @return	void
				 */
				function init()	{
					global $BE_USER,$LANG,$BACK_PATH,$TCA_DESCR,$TCA,$CLIENT,$TYPO3_CONF_VARS;

					parent::init();

                    t3lib_div::mkdir(PATH_site."fileadmin/user_upload/dummy_image");
				}

				/**
				 * Adds items to the ->MOD_MENU array. Used for the function menu selector.
				 *
				 * @return	void
				 */
				function menuConfig()	{
					global $LANG;
					$this->MOD_MENU = Array (
						'function' => Array (
							'1' => $LANG->getLL('title'),
							'2' => $LANG->getLL('about'),
						)
					);
					parent::menuConfig();
				}

				/**
				 * Main function of the module. Write the content to $this->content
				 * If you chose "web" as main module, you will need to consider the $this->id parameter which will contain the uid-number of the page clicked in the page tree
				 *
				 * @return	[type]		...
				 */
				function main()	{
					global $BE_USER,$LANG,$BACK_PATH,$TCA_DESCR,$TCA,$CLIENT,$TYPO3_CONF_VARS;

					// Access check!
					// The page will show only if there is a valid page and if this page may be viewed by the user
					$this->pageinfo = t3lib_BEfunc::readPageAccess($this->id,$this->perms_clause);
					$access = is_array($this->pageinfo) ? 1 : 0;
				
							// Draw the header.
						$this->doc = t3lib_div::makeInstance('mediumDoc');
						$this->doc->backPath = $BACK_PATH;

						$headerSection = $this->doc->getHeader('pages', $this->pageinfo, $this->pageinfo['_thePath']) . '<br />'
							. $LANG->sL('LLL:EXT:lang/locallang_core.xml:labels.path') . ': ' . t3lib_div::fixed_lgd_cs($this->pageinfo['_thePath'], -50);

						$this->content.=$this->doc->startPage($LANG->getLL('title'));
						$this->content.=$this->doc->header($LANG->getLL('title'));
						$this->content.=$this->doc->divider(5);


						// Render content:
						$this->moduleContent();
				
				}

				/**
				 * Prints out the module HTML
				 *
				 * @return	void
				 */
				function printContent()	{
					echo '<div style="padding-left:15px;">'.$this->content.'<div>';
				}

				/**
				 * Generates the module content
				 *
				 * @return	void
				 */
				function moduleContent()	{
                    global $LANG;
					switch((string)$this->MOD_SETTINGS['function'])	{
						case 1:
                            $width=$_REQUEST['width'] ? $_REQUEST['width'] : 460;
                            $height=$_REQUEST['height'] ? $_REQUEST['height'] : 100;
                            $text=$_REQUEST['text']; // ? $_REQUEST['text'] : "Dummy Image";
                            $action=$_REQUEST['action'] ? $_REQUEST['action'] : $LANG->getLL('preview');
                            $bgColor=$_REQUEST['bgColor']; // ? $_REQUEST['bgColor'] : "ccc";
                            $feColor=$_REQUEST['feColor']; // ? $_REQUEST['feColor'] : "ff0000";
                            
                            if($feColor && !$bgColor)
                                $bgColor='ccc';

                            $bgColor=str_replace("#", "", $bgColor);
                            $feColor=str_replace("#", "", $feColor);

                            if($action==$LANG->getLL('save')) {
                                $contents= file_get_contents('http://dummyimage.com/'.$width.'x'.$height.'/'.$bgColor.'/'.$feColor.'&text='.$text);
                                $fileName= $width.'x'.$height.'_'.$text.'.gif';
                                $fileName= str_replace(" ", "_", strtolower($fileName));
                                $fileName= str_replace("č", "c", $fileName);
                                $fileName= str_replace("ž", "z", $fileName);
                                $fileName= str_replace("š", "s", $fileName);
                                $fileName= str_replace("đ", "dz", $fileName);
                                $savefile = fopen(PATH_site.'fileadmin/user_upload/dummy_image/'.$fileName, 'w');
                                fwrite($savefile, $contents);
                                fclose($savefile);
                                $message=$LANG->getLL('imageSaved').'<br />'.$LANG->getLL('path').': fileadmin/user_upload/dummy_image/'.$fileName;
                            }

                            $content.='
                                <form name="dummyImage" method="post">
                                <label for="width">'.$LANG->getLL('width').'(px):</label><br /> <input name="width" type="text" value="'.$width.'" maxlength="5" />
                                
                                <div style="clear:both; height:7px;"></div>
                                <label for="height">'.$LANG->getLL('height').'(px):</label><br /> <input name="height" type="text" value="'.$height.'" maxlength="5"/>
                                
                                <div style="clear:both; height:7px;"></div>
                                <label for="feColor">'.$LANG->getLL('textColor').'(HEX):</label><br /><input name="feColor" maxlength="7" type="text" value="'.$feColor.'" />

                                <div style="clear:both; height:7px;"></div>
                                <label for="bgColor">'.$LANG->getLL('backgroundColor').'(HEX):</label><br /><input maxlength="7" name="bgColor" type="text" value="'.$bgColor.'" />
                                
                                <div style="clear:both; height:7px;"></div>
                                <label for="text">'.$LANG->getLL('text').':</label><br /> <input name="text" type="text" maxlength="100" value="'.$text.'" /><br /><br />
                                
                                <!-- * - '.$LANG->getLL('obligatory').'<br />-->
                                
                                <hr style="margin-bottom:10px;" />
                                <img src="http://dummyimage.com/'.$width.'x'.$height.'/'.$bgColor.'/'.$feColor.'&text='.$text.'" />
                            
                                
                                <div style="clear:both; height:7px;"></div>
                                <input name="action" type="submit" value="'.$LANG->getLL('preview').'" /> <input name="action" type="submit" value="'.$LANG->getLL('save').'" />
                                </form>
                            ';


							$this->content.=$content.'<br />'.$message;
						break;
					    default:
                            die("ERROR");
                    }
				}
				
		}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/fr_dummy_image/mod1/index.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/fr_dummy_image/mod1/index.php']);
}




// Make instance:
$SOBE = t3lib_div::makeInstance('tx_frdummyimage_module1');
$SOBE->init();

// Include files?
foreach($SOBE->include_once as $INC_FILE)	include_once($INC_FILE);

$SOBE->main();
$SOBE->printContent();

?>
